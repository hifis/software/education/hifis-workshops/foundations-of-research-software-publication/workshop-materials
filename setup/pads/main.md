<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

<!--
This markdown file is a template for your main-pad. Please copy and modify the pad.
Content to be changed in the source is indicated by [setup].
-->

[setup]: # (add workshop date)

# Let's Make Your Script Ready for Publication (DD.-DD.MM.YYYY)


> Please note that this is a public pad. Thus, please only share non-sensitive information.

# Open Questions




# Feedback



# Organizational Details

[setup]: # (possible schedule; please modify times and date according to you needs)

## Day 1 - (DD.MM.YYYY)
- 09:00-09:30 Welcome & Introduction
- 09:30-11:00 Theory Part 1
- *11:00-11:15 Break*
- 11:15-12:45 Practice Part 1
- 12:45-13:00 Wrap Up & Feedback Day 1

## Day 2 - (DD.MM.YYYY)
- 09:00-09:30 Welcome & Introduction
- 09:30-11:00 Theory Part 2
- *11:00-11:15 Break*
- 11:15-12:45 Practice Part 2
- 12:45-13:00 Wrap Up & Feedback Day 2

## Before the Workshop

- [ ] If possible, please bring your own script or a smaller, self-written software that you would like to prepare for publication or simply want to improve during the workshop. **You will benefit a lot if you work directly on your project!** Alternatively, we have prepared some tasks that you could work on during the practice phases.
- [ ] Please fill out the pre workshop survey to help us preparing the workshop.
- [ ] Please check your technical setup:

[setup]: # ("please add a link to your preferred video conferencing tool. Also feel free to add system requirements and further links \(e.g. setup guide, FAQs, troubleshooting,...\) )

  - [ ] Please connect to ...
    - Please **use a recent Chromium-based browser** (e.g., Chromium, Microsoft Edge). Other modern browsers (e.g., FireFox) might work as well but we noticed sometimes performance issues.
    - Please avoid using VPN (if you use any) to improve your connection performance.
    - For further information, please see:

[setup]: # (change minimum Python version if necessary) 

  - [ ] Please make sure that your development environment (editor, git client, ...) is prepared.
    If you want to run our example code, you need Python >= 3.7 and you must be able to install Python packages (e.g. via pip) on your computer.

## During the Workshop

- Please mute yourself if you are not talking.
- Please use a headset.
- Please be back in time after breaks / practice phases.
- Please ask your questions in the chat during the presentation.

## Practice Phases

- You work on self-chosen tasks. Please think about the topics you want to work on during the theory blocks.
- At the beginning of each practical block, please briefly state which tasks you want to work on.
- After the practical block, please give a short feedback to the group about your results.
- If you have any questions / need for discussion, you can always contact the instructors.

[setup]: # (insert link to practice pad)
> Later, we work in the [Practice Phase Pad](practice.md) :)

## Workshop Materials

- [Episodes](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/tree/master/episodes)
- [Code Example](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/tree/0-original-source)

# Day 1

## Welcome and Introduction

- Please tell us *in a minute* about your background and interest in research software.
- You can switch on your camera, if you like **:)**


## [Task]: Please think about situations where you wanted to share your code publicly:


### Why did you want to share your code?


### What worked well?


### What obstacles did you encounter?


## Introduction to the Workshop Example
![Closed Source vs. Open Source](https://raw.githubusercontent.com/OpenScienceMOOC/Module-5-Open-Research-Software-and-Open-Source/master/content_development/images/open_research_software_open_source.png)

Image credit: [Open Science MOOC, Module 5 “Open-Research-Software”](https://github.com/OpenScienceMOOC/Module-5-Open-Research-Software-and-Open-Source/blob/master/content_development/MAIN.md#Introduction) (Look there for more information about why to open source your research software.)

### Example Script "Astronaut Analysis"

- Small Python 3 script using pandas and matplotlib.

> Source available at https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/tree/0-original-source

![example plot 1](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/raw/0-original-source/humans_in_space.png)

![example plot 1](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/raw/0-original-source/combined_histogram.png)

![example plot 1](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/raw/0-original-source/boxplot.png)


### Somebody found my paper! How to proceed?

**1. Put your code under version control**
**2. Make sure that your code is in a sharable state**
**3. Add essential documentation**
**4. Add a license**
**5. Make your code citable**
**6. Release your code**


## [Task]: Please think about how you organize your Git repository:

### Which artifacts do you usually store in the repository?



### Which artifacts do you exclude?



## Step 1: Put your code under version control

> You can follow using the [material from episode 1](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/01_put-your-code-under-version-control.md).

### Where should I store my code?

- **Minimum:** Use a local Git repository including some kind of backup
- **Recommended:** Use a code collaboration platform (e.g., GitLab, GitHub)
- Check your organizational policies where to store your code and whether you can use public code collaboration platforms

### What belongs into the repository?

- Everything to make a usable version of your code:
  -  Source code
  -  Documentation
  -  Build scripts
  -  Test cases
  -  Configuration files
  -  Input data
  -  ...
- Avoid adding generated artifacts:
  - Third-party libraries
  - Generated binaries
  - ...
- A `.gitignore` file helps you to control what goes into your repository. [gitignore.io](https://gitignore.io/) allows to generate well suited start configurations.

### What to do if data is too large?

- Recommended max. Git repository size: 1 Gigabyte
- Git extensions handling large data:
  - **Generic:**
    - [git-lfs](https://git-lfs.github.com/) (code collaboration plattform "standard", central storage, usual Git workflow)
    - [git-annex](https://git-annex.branchable.com/) (distributed storage, adapted Git workflow)
  - **Data analytics focused (storage, provenance + other features):**
    - [DVC](https://dvc.org/) (distributed storage, workflow: additional command line tool + Git)
    - [Datalad](http://handbook.datalad.org/en/latest/intro/executive_summary.html) (distributed storage, workflow: new command line tool instead of Git)
- Consider publishing large data sets separately:
  - [re3data.org](http://re3data.org/) provides an overview about public research data repositories
  - Make sure to reference it in the Git repository in a reliable way
- Tools such as [git filter-branch](https://git-scm.com/docs/git-filter-branch) and [BFG Repo-Cleaner](https://rtyley.github.io/bfg-repo-cleaner/) allow you to remove unwanted or too large files from your Git repository history (**use carefully!!**)
 

### Key Points

- Version control helps you to prepare the code for sharing.
- Make sure to put all relevant artifacts into the repository.
- `.gitignore` helps you to specify things that you do not want to share.


## [Task]: Please take a look at the content of our example repository and particularly the `main.py`.

> You can find the code at https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/blob/1-put-into-git/main.py

### What aspects would you change before sharing it?



## Step 2: Make sure that your code is in a sharable state

> You can follow using the [material from episode 2](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/02_make-sure-that-your-code-is-in-a-sharable-state.md).

### General Hints

- Make sure others can run your code:
    - No dependencies on internal ressources (servers, storage , licensed software, ...)
    - No absolute paths
    - Clearly state dependencies
- Organize your code and directory structure
- **Do not share sensitive data like passwords, user accounts, ssh keys, internal IP addresses, etc.**
- Get in touch with user groups and the community.

### Improve your code style

- Strive for understandable code:
    - Apply a code style - consistency is more important than convenience.
    - Use specific and appropriate names for all artifacts. Do not fear refactorings.
    - Do not overcomment.
    - Read code of others for inspiration.
- Try to do pair programmings and reviews (even if it is [with your rubber duck](https://en.wikipedia.org/wiki/Rubber_duck_debugging)).
- **Use code checkers to sanitize your code.**
    - *Linters* or *Checkers* help to find poor code snippets and help to enforce coding styles.
    - Available in many flavors for many (programming) languages.

### Think about testing

- Small tests are done easily but already show effect.
- Automated tests work as an executable documentation.
- The earlier you start, the more tests you have at the end.
- A good starting point for your build automation...

### Key Points

- Make sure that others can (re-)use your code
- Do not share internals and secrets with your code
- Strive for understandable code


## [Task]: Please think about your experiences with code documentation:

### What experiences have you had with good or bad documentation?




### What was important for you as a user?





### What was important for you as a developer?




### What might be important for the researcher in our example?






## Step 3: Add essential documentation

> You can follow using the [material from episode 3](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/03_add-essential-documentation.md).

### Mind your target groups

- Typical perspectives
    - *Users* will need information about how to use your software to (re-)produce certain results.
    - ~~*Developers*~~ *Contributors* will need information about how they can help you to improve the software.
    - Your *primary target group* is never *user* nor *contributor* but always very different.

- Of common interest are:
    - Short software description: What does it do? And why? And maybe even how?
    - Basic instructions: How to get this running? What do I need?
    - License information

- The *users* are usally also interested in:
    - Installation instructions: Can I use it in my favourite environment?
    - Usage instructions: How to run it? What can be controlled? Which data is valid? What interfaces can be used?
    - Feedback: What has changed? How to get help?

- The *contributor* might want to know:
    - Contribution guidelines: How can I add improvments? How to contact developers?
    - Architecture details: How do the pieces fit together? What are the pieces even? And how comes it's not different?
    - Code of conduct


### Typical Documentation Files

- :exclamation: **README** :exclamation:: The front-page of your code you should create in any case.
    - Provide a short overview (information of common interest...)
    - Don't hesitat to use templates or [copy from other projects](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/blob/3-add-docs/README.md).
- **CONTRIBUTING**: Information about how to participate in development.
- **CODE_OF_CONDUCT**: Explains the ground rules for expected behaviour and participation for contributors
- **LICENSE** file or **LICENSES** folder: Shows the license(s) under which the material is provided
- **CHANGELOG**: Explains major changes
- **CITATION**: Explains how to cite the software in a scientific publication

Also check out the [standard readme](https://github.com/RichardLitt/standard-readme) as an inspiration

### Markup Languages

- Do not use Word for documentation.
- Do not use LaTeX for (code) documentation.

- *Markdown* is one of the most popular markup languages.
  <div style="font-size:10pt">
    It initially focused on Web publishing but there are various extensions and tool chains which make it suitable for creation of complex, technical documentation as well.
    However, the existing dialects and tool chains are a bit fragmented.
    But if you focus on Web publishing it is one of the most used solution today.
  </div>

- *AsciiDoc* provides an easy to read and write syntax, a rich feature set for creation of complex, technical documentation, as well as a rich ecosystem.

- *ReStructuredText* became popular in the Python community.
  <div style="font-size:10pt">
    In combination with its documentation generator Sphinx it is well suited for creation of complex, technical documentation.
  </div>

### Key Points

- Provide documentation for relevant target groups
- Add a `README` file as a minimum documentation artifact to your repository

## Practice Phase Day 1

[setup]: # (insert link to practice pad)
Now, we work in the [Practice Phase Pad](practice.md) :)


## Wrap Up & Feedback Day 1




# Day 2

## Welcome and Introduction

## [Task]: Please think about your experiences with software licenses:

### Which software licenses are you typically using for your code?




### Do you consider the software license when selecting a third-party libraries? Under which licenses are these libraries?




### How do you annotate copyright and license information to your source code?





## Step 4: Add a license
> **DISCLAIMER:** This information is no legal advice and solely reflects the experiences of the episode contributors. Please contact a lawyer or your organizational legal department, if you are in doubt and require solid legal advice.


### Copyright

- Copyright:
  - protects software as source code, object code, or integrated with hardware including documentation and accompanying material
  - grants **exclusive rights** to the copyright holder (e.g., use, copy, distribute)

- Everyone contributing to a software is considered a copyright holder of the software 
  - All contributors are considered as copyright holders
  - A company paying an employed developer obtains most of the exclusive rights


### Software Licenses

- Software licenses are a standardized way to grant rights to others
- Two aspects:
  - Licenses grant certain rights
  - Licenses demands certain obligations (e.g., attribution, disclosure of source code). **Usually, when you distribute the code to third-parties!**
- **Important:**
  - Only use code which is covered by a license
  - Make sure that you cover your code under a suitable license!


### Software License Types

![](https://s3.desy.de/hackmd/uploads/upload_621ca7daaa1da8b3ba42a36fe81fd369.png)


- **Proprietary Software Licenses:** Non-standard licenses with specific licensing
- **Public Domain:** Copyright holders waive all their exclusive rights
- **Free and Open Source Software Licenses:** Imply the availability of the source code and allow open distribution, modification, and re-use
  - **Copyleft**: "The world is evil." (e.g., GPL-2.0, GPL-3.0) => disclose all source code of a distributed derivative work (viral effect)
  - **Permissive** (e.g., MIT, BSD-3, Apache License 2.0: "The world is good." => only a few obligations, licenses are compatible and interoperable with most other licenses

### Combining Modules under Different Licenses

#### Derivative Work or Combined Work?

- Important questions when dealing with **copyleft licenses**
- It depends on:
  - the concrete license and its definition of a derivative work => GPL is quite strict
  - the usage mechanism => copying, static linking, dynamic linking

![](https://upload.wikimedia.org/wikipedia/commons/6/6b/Software-license-compatiblity-graph.svg)

- Strong copyleft licenses (e.g., GPL) make it hard to "achieve" a combined work!


#### License Incompatibility

- Exists when a program is a derivative work of components licensed under conflicting licensing terms.
- Typical problem with copyleft licenses
- Modules under conflicting licenses cannot be legally combined and distributed

![](https://upload.wikimedia.org/wikipedia/commons/2/2b/Floss-license-slide-image.svg)


> - Make sure to check the license of libraries that you use!
> - Prefer using official, permissive FOOS licenses: https://opensource.org/licenses/alphabetical


### Minimal Checklist

- Choose a license:
  - Depends on different factors but https://choosealicense.com/ helps to get started
  - Analyze / comply with licenses of third-party dependencies (requires list of dependencies, their version, their license, the way you use them)
  - Ask your boss for permission

- Prepare your code:
  - Minimum:
    - Add a license file and state the copyright holder
  - Recommended: 
     - Add license file(s) to your repository (=> `LICENSES` directory)
     - Add a copyright/license information to all your files
     - Provide a license and copyright holder hint
     - Document your third-party dependencies including their licenses


> Ask for legal advice if you are unsure!

### Key Points

- Minimum: Add a license file and state the copyright holder
- Recommended: Follow REUSE Specification Version 3.0.0
- Consider third-party licenses from the very beginning



## [Task]: Please think about your experiences concerning attribution of software in research publications:

### Did you cite or reference software that you used? Why or why not?





### How did you cite or reference software?





## Step 5: Make your code citable

### How to cite Software?

- Cite all software packages (including your own) in the reference list of your academic work.
- Ideally try to:
  - Cite the **software itself**
  - Cite the **exact version** of the software
  - Cite the software using its **unique identifier**
  - Cite the **source code**
  - Cite the **authors** of the software
  - Cite the **release date** of the software
- For more information see: [Research software citation for researchers](https://research-software.org/citation/researchers/)


#### Example

>"The data sets and the notebook containing the analysis details have been published separately [11]."
>
> ### References
> - ...
> - [11] Schlauch, Tobias & Haupt, Carina. (2019). Analysis of the DLR Knowledge Exchange Workshop Series on Software Engineering (Version 1.2.0). Zenodo. https://doi.org/10.5281/zenodo.3403991



### How to make your Software citable?

- Please make sure that other persons can easily cite your software by:
  - Providing citation metadata,
  - Archiving your software and obtaining a persistent identifier (PID) for it as well as
  - Providing a prominent citation hint as part of your documentation.
- (Citation) Metadata Formats:
  - [Citation File Format](https://citation-file-format.github.io/cff-initializer-javascript/) (emerging standard for software citation metadata)
  -  [CodeMeta](https://codemeta.github.io/codemeta-generator/)
- Practical approaches:
  - Add citation metadata and archive the release snapshot in an (open) digital repository (e.g., Zenodo): [Example 1](https://doi.org/10.5281/zenodo.3403991), [Example 2](https://doi.org/10.5281/zenodo.5009043)
  - Add citation metadata and archive via Software Heritage: [Example 3](https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://github.com/DLR-SC/analysis-dlr-se-waws)


### Software Journals

- If your organization only "awards" peer-reviewed publications, software journals might be an option
- Recommendation: [Journal of Open Source Software](https://joss.theoj.org/) (JOSS)
  - Focused on research software tools
  - Developer-friendly 
  - Ensures that your software itself is citable
- [Overview of Software Journals](https://www.software.ac.uk/which-journals-should-i-publish-my-software)


### Key Points

- Cite all relevant software packages as good as possible in your academic work
- Make your code citable by adding citation metadata and archiving your software
- Encourage citation of your software


## [Task]: Please think about how you handle working versions your software:

### How do you remember or mark these special versions?





### How do you share these special versions with others?





## Step 6: Release your code


### Release Basics

- A *release* is a specific working software version.
- The *release number* uniquely identifies the release.
  - [Semantic Versioning](https://semver.org/) (e.g., `1.0.1`)
  - [Calendar Versioning](https://calver.org/) (e.g., `2021-04-22`).
- A *release tag* marks the release in your source code repository.
- The *changelog* documents all notable changes ([keep a changelog](https://keepachangelog.com/)).
- A user uses the *release package* to install and use the release:
  - Contains code + documentation
  - Simplest form: snapshot of your source code repository packaged as ZIP file


### Minimal Release Checklist for Research Code

Before your start, please make sure that you:

- Defined which **release number scheme** you want to use. => Code Example: Semantic Versioning
- Established the **name mapping between release number and release tags**. => Code Example: Same as the release number
- Defined how you handle **citation metadata**. => Code Example: Zenodo (+ Citation File Format in a later workshop version)
- Defined **which archive** you want to use. => Code Example: Zenodo
- Defined the **format and content of your release package(s)**. => ZIP file snapshot

#### 1. Prepare your code for release

- Define the **release number** for the release. => Code Example: `1.0.0`
- Document the user-visible changes in the **changelog**. => Code Example: Add a minimal changelog
- **Update the citation metadata** for this release. => Code Example: Zenodo (update `CITATION.cff` file in a newer workshop version)

#### 2. Check your code

- **Make sure that your code "works".** => Code Example: Run `test.sh` + validate manually
- **Review your documentation.** => Code Example: Check manually again

#### 3. Publish and archive the release

- **Mark the release** in the source code repository using a (Git) tag. => Code Example: Create in via Git/GitLab
- **Create the release package(s)** and eventually publish it on a code distribution platform. => Code Example: Download Zip file from GitLab
- **Archive the release package(s).**  => Code Example: Upload the ZIP file to the Zenodo deposit

### Key Points

- Mark used, working software versions as releases using release numbers and tags
- Document important changes in a changelog
- Archive the release package


## Practice Phase Day 2

[setup]: # (insert link to practice pad)
Now, we work in the [Practice Phase Pad](practice.md) :)


## Wrap Up & Feedback Day 2




## Wrap up

