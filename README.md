<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Foundations of Research Software Publication

In this workshop you learn about the basic steps to prepare your code for sharing with others and make it ready for citation in a research paper.
The workshop example is the [astronauts analysis code](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis).
A small data analysis script which we want to publish as supplementary material for one of our research papers.

The workshop is partially inspired by the TIB workshop "FAIR Data and Software":

> Leinweber Katrin, Kraft Angelina, Kuzak Mateusz, Johnston Luke, Hammitzsch Martin & Förstner Konrad. (2018, July). FAIR Data and Software: A Carpentries-based workshop at TIB, Hannover (Version 1.0.0). Zenodo. http://doi.org/10.5281/zenodo.3707745

## Prerequisites

- Basic knowledge about Git and a collaboration platform such as GitLab or GitHub.
- Basic knowledge in using a programming language such as Python, R, or Matlab.

## Setup

- Bring your computer including a Web browser, a text editor, and a Git client.
- Bring your analysis script or other small software that you would like to prepare for publication including your development environment.
- Optional: If you want to run the code example,
  you require Python >= 3.6.1 and you have to be able to install Python packages (e.g., via pip).

## Curriculum

- [Step 1: Put your code under version control](episodes/01_put-your-code-under-version-control.md)
- [Step 2: Make sure that your code is in a sharable state](episodes/02_make-sure-that-your-code-is-in-a-sharable-state.md)
- [Step 3: Add essential documentation](episodes/03_add-essential-documentation.md)
- [Step 4: Add a license](episodes/04_add-license.md)
- [Step 5: Make your code citable](episodes/05_make-your-code-citable.md)
- [Step 6: Release your code](episodes/06_release-your-code.md)

If you want to set up a workshop based on this materials, please see the [Workshop Setup Information](setup/README.md).

## Schedule

The actual schedule may vary slightly depending on the interests of the participants.

| Time | Topic | Remarks |
| ------ | ------ | ------ |
|  | [Setup](#Setup) |  |
| 09:00 - 09:30 | Welcome |  |
| 09:30 - 11:00 | Theory Part 1 | put your code into Git, clean up code, add essential documentation |
| 11:00 - 12:30 | Practice Part 1 | practice using your code or our code example |
| 12:30 - 13:30 | *Lunch Break* |  |
| 13:30 - 15:00 | Theory Part 2 | add license information, prepare for release, prepare for citation |
| 15:00 - 16:45 | Practice Part 2 | practice using your code or our code example |
| 16:45 - 17:00 | Wrap Up & Feedback |  |

## Contributors

Here you find the main contributors to the material:

- Tobias Schlauch
- Martin Stoffers
- Carina Haupt
- Michael Meinel
- Katrin Leinweber
- Stephan Druskat
- Benjamin Wolff

## Contributing

Please see [the contribution guidelines](CONTRIBUTING.md) for further information about how to contribute.

## Changes

Please see the [Changelog](CHANGELOG.md) for notable changes of the material.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
